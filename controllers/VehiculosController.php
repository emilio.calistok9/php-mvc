<?php
class VehiculosController
{

    public function index()
    {
        require_once("models/VehiculosModel.php");
        $vehiculos = new VehiculosModel();

        $data["titulo"] = "Vehiculos";
        $data["vehiculos"] = $vehiculos->get_Vehiculos();

        require_once("views/vehiculos/VehiculosView.php");
    }

    public function nuevo()
    {
        $data["titulo"] = "Agregar Vehiculo";
        // Retorna la vista
        require_once("views/vehiculos/VehiculoNuevoView.php");
    }
}
