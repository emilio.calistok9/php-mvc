<?php

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $data["titulo"]; ?></title>
</head>

<body>
    <h2><?php echo $data["titulo"]; ?></h2>

    <form id="nuevo" name="nuevo" method="POST" autocomplete="off" action="">
        <div class="form-group">
            <label for="placa">Placa</label>
            <input type="text" id="placa" name="placa" class="form-control" placeholder="Nombre Patente" autofocus>
        </div>
        <div class="form-group">
            <label for="marca">Marca</label>
            <input type="text" id="marca" name="marca" class="form-control" placeholder="Nombre Patente" autofocus>
        </div>
        <div class="form-group">
            <label for="modelo">modelo</label>
            <input type="text" id="modelo" name="modelo" class="form-control" placeholder="Nombre Patente" autofocus>
        </div>
        <div class="form-group">
            <label for="anio">Año</label>
            <input type="text" id="anio" name="anio" class="form-control" placeholder="Nombre Patente" autofocus>
        </div>
        <div class="form-group">
            <label for="color">Color</label>
            <input type="text" id="color" name="color" class="form-control" placeholder="Nombre Patente" autofocus>
        </div>
        <button id="guardar" name="guardar" type="submit">Guardar</button>
    </form>

</body>

</html>