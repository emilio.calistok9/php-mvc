<?php

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $data["titulo"]; ?></title>
</head>

<body>
    <h2><?php echo $data["titulo"]; ?></h2>

    <br>
    <a href="">Agregar</a>
    <br>

    <table border="1" width="100">
        <thead>
            <tr>
                <th>Placa</th>
                <th>Marca</th>
                <th>Modelo</th>
                <th>Año</th>
                <th>Color</th>
            </tr>
        </thead>
        <tbody>

            <?php foreach ($data["vehiculos"] as $row) {
                echo "<tr>";
                echo "<td>" . $row["placa"] . "</td>";
                echo "<td>" . $row["marca"] . "</td>";
                echo "<td>" . $row["modelo"] . "</td>";
                echo "<td>" . $row["anio"] . "</td>";
                echo "<td>" . $row["color"] . "</td>";
                echo "</td>";
            } ?>

        </tbody>
    </table>

</body>

</html>