<?php
require_once("config/Config.php");
require_once("core/Routes.php");
require_once("config/Database.php");
require_once("controllers/VehiculosController.php");

if (isset($_GET['c'])) {
    $controlador = cargarControlador($_GET['c']);

    // Validamos que contenga la acción en la url
    if (isset($_GET['a'])) {
        cargarAccion($controlador, $_GET['a']);
    } else {
        cargarAccion($controlador, ACCION_PRINCIPAL);
    }
} else {
    // Llamada el controlador por default
    $controlador = cargarControlador(CONTROLADOR_PRINCIPAL);
    cargarAccion(CONTROLADOR_PRINCIPAL, ACCION_PRINCIPAL);
}

$control = new VehiculosController();
$control->index();
