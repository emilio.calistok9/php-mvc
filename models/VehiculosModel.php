<?php
class VehiculosModel
{
    private $db;
    private $vehiculos;

    public function __construct()
    {
        $this->db = Conectar::conexion();
    }

    public function get_Vehiculos()
    {
        $query = "SELECT * FROM vehiculos";
        $resultado = $this->db->query($query);

        while ($row = $resultado->fetch_assoc()) {
            $this->vehiculos[] = $row;
        }

        return $this->vehiculos;
    }

    public function create_Vehiculos($placa, $marca, $modelo, $anio, $color)
    {
        $query = "INSERT INTO vehiculos (placa, marca, modelo, anio, color) VALUES ('$placa', '$marca', '$modelo', '$anio', '$color')";
        $resultado = $this->db->query($query);
    }
}
