<?php
function cargarControlador($controlador)
{
    // ucwords => convierte la primera letra en mayúscula
    $nombreControlador = ucwords($controlador) . "Controller";
    $archivoControlador = 'controllers/' . ucwords($controlador) . '.php';

    // Si no encuentra el archivo, llama al controlador principal
    if (!is_file($archivoControlador)) {
        $archivoControlador = 'controllers/' . CONTROLADOR_PRINCIPAL . '.php';
    }

    require_once $archivoControlador;
    $control = new $nombreControlador();
    return $control;
}

function cargarAccion($controlador, $accion)
{
    if (isset($accion) && method_exists($controlador, $accion)) {
        // Invocamos un método específico de un controlador
        $controlador->$accion();
    } else {
        $controlador->ACCION_PRINCIPAL();
    }
}
